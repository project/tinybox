<?php


function tinybox_settings() {
  $form['general'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['general']['tinybox_splash_nid'] = array(
    '#type' => 'textfield',
    '#title' => t('NID for Splash'),
    '#default_value' => variable_get('tinybox_splash_nid', ''),
    '#description' => t('Leave blank to disable Splash. If you enter NID here then the node will show as Splash screen on frontpage load.'),
  );

  return system_settings_form($form);
}

